DROP TABLE IF EXISTS commande, fournisseur, produit;

CREATE TABLE fournisseur (
    fno serial,
    nom char(15) NOT NULL,
    prenom char(10) NOT NULL,
    adresse char(50),
    tel char(10),
    CONSTRAINT pk_f PRIMARY KEY (fno)
);

CREATE TABLE produit (
    pno serial,
    libelle char(10) NOT NULL,
    couleur char(10) NOT NULL,
    poids int,
    CONSTRAINT pk_p PRIMARY KEY (pno)
);

CREATE TABLE commande (
    fno int,
    pno int,
    prix int,
    quantite int,
    CONSTRAINT pk_c PRIMARY KEY (fno, pno),
    CONSTRAINT fk_fno FOREIGN KEY (fno) REFERENCES fournisseur (fno) ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT fk_pno FOREIGN KEY (pno) REFERENCES produit (pno) ON UPDATE CASCADE ON DELETE CASCADE
);

INSERT INTO fournisseur (nom, prenom) VALUES ('Dupont', 'Paul'), ('Durant', 'Pierre'), ('Lefebvre', 'Jean');
INSERT INTO produit (libelle, couleur) VALUES ('Chaise', 'rouge'), ('Bureau', 'gris'), ('Armoire', 'Blanc');
INSERT INTO commande
    SELECT f.fno, p.pno, RANDOM()*1000, RANDOM()*20
    FROM fournisseur AS f, produit AS p;

SELECT *
FROM commande
NATURAL JOIN fournisseur
NATURAL JOIN produit;
