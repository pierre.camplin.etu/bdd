DELETE FROM aeroport;
DELETE FROM ligne;
DELETE FROM avion;
DELETE FROM pilote;
DELETE FROM vol;

TRUNCATE pilote RESTART IDENTITY CASCADE;

INSERT INTO avion VALUES (100, 'B747', 350, 'AIR FRANCE') ;

INSERT INTO avion (ano, type, places) VALUES (102, 'A320', 200) ;

INSERT INTO avion (ano, type, compagnie) VALUES (103, 'B747', 'TWA') ;

INSERT INTO avion (compagnie, places, type, ano) VALUES ('AIR FRANCE', 500, 'B747', 104) ;

INSERT INTO aeroport (rno, libelle, ville) VALUES ('CDG', 'Roissy-Charles de Gaulle', 'Paris');

INSERT INTO aeroport (rno, ville, libelle) VALUES ('MAD', 'Madrid Barajas', 'Madrid');

INSERT INTO aeroport (rno, ville) VALUES ('SDN', 'Sydney');

INSERT INTO pilote (nom, prenom, adressep) VALUES ('Szut', 'Piotr', 'Estonie');

INSERT INTO pilote (adressep, nom, prenom) VALUES('USA', 'Taylor', 'Frank');

INSERT INTO ligne (lno, arrivee) VALUES (714, 'SDN') ;

INSERT INTO ligne VALUES (314, 'CDG', 'MAD') ;

INSERT INTO vol (lno, pno, ano) VALUES (714, 1, 102);

UPDATE vol
SET hdepart = '11:00', harrivee='17:25'
WHERE hdepart IS NULL ;

UPDATE vol
SET ano = 103
WHERE lno = 314 AND pno = 3;

UPDATE ligne SET lno = 42
WHERE depart = 'CDG' and arrivee = 'MAD' ;

UPDATE aeroport
SET rno = 'SYD', libelle='Sydney-Kingsford Smith'
WHERE rno = 'SDN' ;

UPDATE aeroport
SET libelle=ville, ville=libelle
WHERE rno = 'MAD' ;

DELETE FROM avion WHERE compagnie LIKE '%FRANCE' ;

DELETE FROM pilote WHERE nom = 'Szut' ;

DELETE FROM aeroport WHERE rno = 'SYD' ;
