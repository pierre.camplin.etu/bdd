DROP TABLE IF EXISTS aeroport, avion, ligne, pilote, vol;

CREATE TABLE aeroport (
    rno char(3),
    libelle char(30),
    ville char(20),
    adresseA char(50),
    CONSTRAINT pk_aero PRIMARY KEY(rno)
);

CREATE TABLE avion (
    ano int,
    type char(10),
    places int CHECK(places BETWEEN 100 AND 500),
    compagnie char(20),
    CONSTRAINT pk_avion PRIMARY KEY(ano)
);

CREATE TABLE pilote (
    pno serial PRIMARY KEY,
    nom char(20) NOT NULL,
    prenom char(15) NOT NULL,
    adresseP char(50) NOT NULL
);

CREATE TABLE ligne (
    lno int,
    depart char(3),
    arrivee char(3),
    CONSTRAINT pk_ligne PRIMARY KEY(lno),
    CONSTRAINT fk_depart FOREIGN KEY(depart) REFERENCES aeroport(rno) ON UPDATE CASCADE ON DELETE SET NULL,
    CONSTRAINT fk_arrivee FOREIGN KEY(arrivee) REFERENCES aeroport(rno) ON UPDATE CASCADE ON DELETE SET NULL
);

CREATE TABLE vol (
    lno int,
    ano int,
    pno int,
    hdepart time,
    harrivee time,
    CONSTRAINT pk_vol PRIMARY KEY (lno, ano, pno),
    CONSTRAINT fk_lno FOREIGN KEY (lno) REFERENCES ligne (lno) ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT fk_ano FOREIGN KEY (ano) REFERENCES avion (ano) ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT fk_pno FOREIGN KEY (pno) REFERENCES pilote (pno) ON UPDATE CASCADE ON DELETE CASCADE
);
