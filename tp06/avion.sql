DROP TABLE IF EXISTS aeroport, avion, ligne, pilote, vol, noms, prenoms,
                     adresses, compagnies, modeles, chomeurs;
DROP SEQUENCE IF EXISTS numeros;

CREATE TABLE aeroport (
    rno char(3),
    libelle char(30),
    ville char(20),
    adresseA char(50),
    CONSTRAINT pk_aero PRIMARY KEY(rno)
);

CREATE TABLE avion (
    ano int,
    type char(10),
    places int CHECK(places BETWEEN 100 AND 500),
    compagnie char(20),
    CONSTRAINT pk_avion PRIMARY KEY(ano)
);

CREATE TABLE pilote (
    pno serial PRIMARY KEY,
    nom char(20) NOT NULL,
    prenom char(15) NOT NULL,
    adresseP char(50) NOT NULL
);

CREATE TABLE ligne (
    lno int,
    depart char(3),
    arrivee char(3),
    CONSTRAINT pk_ligne PRIMARY KEY(lno),
    CONSTRAINT fk_depart FOREIGN KEY(depart)
        REFERENCES aeroport(rno)
        ON UPDATE CASCADE
        ON DELETE SET NULL,
    CONSTRAINT fk_arrivee FOREIGN KEY(arrivee)
        REFERENCES aeroport(rno)
        ON UPDATE CASCADE
        ON DELETE SET NULL
);

CREATE TABLE vol (
    lno int,
    ano int,
    pno int,
    hdepart time,
    harrivee time,
    CONSTRAINT pk_vol PRIMARY KEY (lno, ano, pno),
    CONSTRAINT fk_lno FOREIGN KEY (lno)
        REFERENCES ligne (lno)
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT fk_ano FOREIGN KEY (ano)
        REFERENCES avion (ano)
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT fk_pno FOREIGN KEY (pno)
        REFERENCES pilote (pno)
        ON UPDATE CASCADE
        ON DELETE CASCADE
);

CREATE TEMP TABLE noms (nom char(15));
CREATE TEMP TABLE prenoms (prenom char(15));
CREATE TEMP TABLE adresses (adresse char(20));

ALTER SEQUENCE pilote_pno_seq RESTART 100;

INSERT INTO noms VALUES ('Haddock'), ('Tournesol'), ('Bergamotte'), ('Lampion');
INSERT INTO prenoms VALUES ('Tryphon'), ('Archibald'), ('Hippolyte'), ('Séraphin');
INSERT INTO adresses VALUES ('Moulinsart'), ('Sbrodj'), ('Bruxelles'), ('Klow');

INSERT INTO pilote(nom, prenom, adressep)
     SELECT noms.nom, prenoms.prenom, adresses.adresse
     FROM noms, prenoms, adresses;

INSERT INTO avion VALUES(105, 'A320', 300, 'AIR FRANCE');
INSERT INTO avion VALUES(106, 'A380', 320, 'LUFTHANSA');
INSERT INTO avion VALUES(107, 'B747', 500, 'AIR FRANCE');
INSERT INTO avion VALUES(108, 'A320', 300, 'TWA');
INSERT INTO avion VALUES(109, 'B747', 450, 'PANAM');
INSERT INTO avion VALUES(110, 'A320', 300, 'IBERAMIA');
INSERT INTO avion VALUES(111, 'A320', 300, NULL);

CREATE TABLE compagnies AS
    SELECT DISTINCT compagnie
    FROM avion
    WHERE compagnie IS NOT NULL;

CREATE TABLE modeles AS
    SELECT type, MAX(places) AS places
    FROM avion
    GROUP BY type;

CREATE SEQUENCE numeros START 130;

INSERT INTO avion(ano, type, places, compagnie)
    SELECT nextval('numeros'), modeles.type, modeles.places, compagnies.compagnie
    FROM modeles, compagnies;

INSERT INTO aeroport(rno, libelle, ville)
    VALUES ('JFK', 'John Fitzgerald Kennedy', 'New-York') ;
INSERT INTO aeroport(rno, libelle,ville)
    VALUES ('CDG', 'Roissy Charles de Gaulles','Paris') ;
INSERT INTO aeroport(rno, libelle,ville)
    VALUES ('MAD', 'Madrid Barajas','Madrid') ;
INSERT INTO aeroport(rno, libelle) VALUES ('BRU', 'Bruxelles') ;
INSERT INTO aeroport(rno, libelle) VALUES ('GVA', 'Genève') ;
INSERT INTO aeroport(rno, libelle, ville)
    VALUES ('ORY', 'Orly', 'Paris') ;
INSERT INTO aeroport(rno, libelle) VALUES ('LAI', 'Lannion') ;
INSERT INTO aeroport(rno, libelle, ville)
    VALUES ('LIL', 'Lille-Lesquin', 'Lille') ;

UPDATE aeroport
    SET ville = libelle
    WHERE ville IS NULL;

ALTER SEQUENCE numeros RESTART 1000;

INSERT INTO ligne(lno, depart, arrivee)
    SELECT nextval('numeros'), a1.rno, a2.rno
    FROM aeroport AS a1 JOIN aeroport AS a2
        ON a1.rno <> a2.rno;

INSERT INTO vol (lno, ano, pno)
    SELECT l.lno, a.ano, p.pno
    FROM ligne AS l, (
        SELECT ano
        FROM avion
        WHERE compagnie = 'AIR FRANCE'
        AND type LIKE 'A%'
    ) AS a, (
        SELECT pno
        FROM pilote
        WHERE pno % 7 = 0
    ) AS p
    WHERE l.depart IN ('CDG', 'JFK', 'MAD')
        OR l.arrivee IN ('CDG', 'JFK', 'MAD');

UPDATE vol
SET hdepart = '14:10', harrivee = '16:25'
WHERE pno = 126
    AND ano = (SELECT ano FROM avion WHERE compagnie = 'AIR FRANCE' AND type = 'A380')
    AND lno = (SELECT lno FROM ligne WHERE depart = 'CDG' AND arrivee = 'JFK');

CREATE TABLE chomeurs AS (
    SELECT *
    FROM pilote
    WHERE pno NOT IN (SELECT pno FROM vol)
);

DELETE FROM pilote
WHERE pno IN (
    SELECT pno
    FROM chomeurs
);
